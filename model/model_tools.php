<?php 
//------------------------------------------
//
// Project 2 model tools for route paths
//
//------------------------------------------
require_once("../includes/site_constants.php");
require_once("model.php");

//if (DEBUG) echo "<br />In model_tools.php<br />";

//--------------------------------------------------
//
// load_route_data puts the routing data of the 
// given uri and sets the information up for
// insertion into the route_paths table
//
function load_route_data(&$uri)
{
	$r_xml = simplexml_load_file($uri);
	$number = 0;
  $nstations = 0;
  foreach ($r_xml->xpath("/root/routes/route") as $item)
  {
    foreach ($item as $key=>$value)
    {
			if ($key != "config")
      { 
				switch($key)
				{
  				case "num_stns":
	  				$nstations = $value;
		  			break;
  				case "number":
	  				$number = $value;
		  			break;
				  default:
					  break;
				}
      }
      else if ($key == "config")
      {
				foreach($r_xml->xpath("/root/routes/route/config") as $path)
        {
          $i = 0; //initialize index variable
          foreach($path as $st=>$st_name)
          {
						 $stn_list["$i"] = $st_name;
             $i++;
					}
          $rt_path = "";
					foreach($stn_list as $station)
          {
						  if (DEBUG) {echo "Station: $station<br />";}
              $rt_path .= $station . ","; //prepare csv value
          }
          // truncate trailing comma
          $rt_path = substr($rt_path,0,mb_strlen($rt_path) - 1);
				}
				// if (DEBUG) {echo "route number: $number, route path: $rt_path<br />";}
      }
    }
		//if (DEBUG) {echo "route number: $number, route path: $rt_path<br />";}
  	putin_rpaths($number,$rt_path,$nstations);
  }
}



//------------------------------------------------------------------
//
//  get_dinfo loads the bart api departure information (represented
//    in an xml file) based on the station. This function returns
//    the destination point and the estimated minutes until the
//    departure time.
//
function get_dinfo($stn)
{
		 $uri = BART_ETD . $stn;

	   $t_xml = simplexml_load_file($uri);
     $dlist = null;
		 foreach($t_xml->xpath("/root/station/etd") as $dst)
     {
        foreach($dst as $d=>$value)
        {
				  switch($d)
				  {
						 case "destination":
							 $dest = "$value";
       			   break;
  				   case "estimate":
               foreach($value as $attr=>$v)
               {
                 switch($attr)
                 {
		   						 case "minutes":
                     if ($v == "Leaving")
                     {
											 $etd = $v . " ";
                     }
                     else 
                     {
                       $etd = $v . " minutes";
                     }
                     break;
								   default:
									   break;
                 }
                 break;
               }
		  		     break;
				     default:
					     break;
				  }
          if (isset($dest) && isset($etd))
          {
						  $dlist["$dest"] = $etd;
          }
					if ($d == "estimate")
					{
            break; // out of loop, only get first val of minutes
					}
       }   
     }
		 return $dlist;
}



//----------------------------------------------------------
//
// putin_rpaths inserts number of stations and route number
// and the list of stations on the path into route_paths 
// table
//
function putin_rpaths(&$rnum,&$path,&$nstations)
{
	$rt_found = duplicate_route($rnum);
	if (DEBUG) echo "in putin_rpaths: $rnum, $path, $nstations<br />";

  if ($rt_found == 0) // abbr not present in table so insert
  {
		$dbh = connect_to_db();
		//	if (DEBUG) echo "in putin_rpaths: $rnum, $path, $nstations<br />";

		$dbh->beginTransaction();
		try 
		{
      // insert abbr, route_name and rourte id into table 
      $ins1 = $dbh->prepare("
        INSERT INTO route_paths 
        (nstations,path_list,route_num) 
        VALUES (:nstations,:path,:rnum);
      ");
      $ins1->bindValue(':nstations',(int)$nstations,PDO::PARAM_INT);
      $ins1->bindValue(':path',$path,PDO::PARAM_STR);
      $ins1->bindValue(':rnum',(int)$rnum,PDO::PARAM_INT);
      $ins1->execute();
      $dbh->commit();
		}
		catch(PDOException $e) 
		{
			echo $e->getMessage();
			$dbh->rollBack();
		}
		$ins1 = null;
    $dbh = null;
  }

  $odbh = connect_to_db();
  $rquery = $odbh->prepare("     
    SELECT id FROM route_paths 
    WHERE route_num=:rnum LIMIT 1
   ");
  $rquery->bindValue(':rnum',$rnum,PDO::PARAM_INT);
  $rquery->execute();
	$row = $rquery->fetch(PDO::FETCH_ASSOC);
  $rquery = null;
  $odbh = null; 

 	return (empty($row["id"])) ? null : $row["id"]; 
}


//--------------------------------------------------------
//
// duplicate_route returns 0 if route number 
// is already present in table, otherwise returns 
// route number.
//
function duplicate_route(&$rnum)
{
  $dbh = connect_to_db();
  if (DEBUG) echo "<br />duplicate_route<br />";

  $search_id   = $dbh->prepare("
    SELECT route_num FROM route_paths
    WHERE route_num=:rnum LIMIT 1
   ");   
  $search_id->bindValue(':rnum',(int)$rnum,PDO::PARAM_INT);
  $search_id->execute();
  $row = $search_id->fetch(PDO::FETCH_ASSOC);

  echo "<br />route_id in duplicate_route: " . $row["route_num"] . "<br />";  
  //close connections
  $search_id = null;
  $dbh = null;

 	return (empty($row["route_num"])) ? 0 : $row["route_num"]; 
}


//--------------------------------------------------
//
// get_routes returns a list of the available
// routes on the BART system
//
function get_routes()
{
  $dbh = connect_to_db();

  $results = $dbh->prepare("
    SELECT roid 
    FROM route_names
    ");
  $results->execute();
  $rows = $results->fetchAll(PDO::FETCH_ASSOC);

  $dbh = null; // close connection
  $results = null;

  return $rows;
}


//--------------------------------------------------
//
// get_stn_coords returns the coordinates of
// the given station
//
function get_stn_coords(&$stn)
{
  $dbh = connect_to_db();

  $results = $dbh->prepare("
    SELECT stn_lat, stn_long 
    FROM stations
    WHERE stn_abbr = :stn LIMIT 1
    ");
  $results->bindValue(':stn',$stn,PDO::PARAM_STR);
  $results->execute();
  $rows = $results->fetchAll(PDO::FETCH_ASSOC);
	//  if (DEBUG){echo "in get_stn_coords: ";print_r($rows);echo "<br/>";}

  $dbh = null; // close connection
  $results = null;

  return $rows;
}


//--------------------------------------------------
//
// get_rinfo returns basic info of
// the given route
//
function get_rinfo(&$r)
{
  $dbh = connect_to_db();

  $results = $dbh->prepare("
    SELECT abbr, rname, color 
    FROM route_names
    WHERE roid = :r LIMIT 1
    ");
  $results->bindValue(':r',$r,PDO::PARAM_INT);
  $results->execute();
  $rows = $results->fetch(PDO::FETCH_ASSOC);
//	if (DEBUG){echo "in get_info: ";print_r($rows);echo "<br/>";}

  $dbh = null; // close connection
  $results = null;

  return $rows;
}


//--------------------------------------------------
//
// get_stn_name returns the full station name given
// the station's abbreviation
//
function get_stn_name(&$abbr)
{
  $dbh = connect_to_db();

  $results = $dbh->prepare("
    SELECT stn_name 
    FROM stations
    WHERE stn_abbr = :abbr LIMIT 1
    ");
  $results->bindValue(':abbr',$abbr,PDO::PARAM_INT);
  $results->execute();
  $row = $results->fetch(PDO::FETCH_ASSOC);
//	if (DEBUG){echo "in get_info: ";print_r($rows);echo "<br/>";}

  $dbh = null; // close connection
  $results = null;

  return $row["stn_name"];
}


//--------------------------------------------------
//
// get_stinfo returns list of stations
// of given route
//
function get_stinfo(&$r)
{
  $dbh = connect_to_db();

  $results = $dbh->prepare("
    SELECT nstations, path_list
    FROM route_paths
    WHERE route_num = :r LIMIT 1
    ");
  $results->bindValue(':r',$r,PDO::PARAM_INT);
  $results->execute();
  $rows = $results->fetch(PDO::FETCH_ASSOC);
//	if (DEBUG){echo "in get_info: ";print_r($rows);echo "<br/>";}

  $dbh = null; // close connection
  $results = null;

  return $rows;
}


//--------------------------------------------------
//
// get_stcoords returns the latitude and longitude
// coordinates of a given station
//
function get_stcoords(&$st)
{
  $dbh = connect_to_db();

  $results = $dbh->prepare("
    SELECT stn_abbr, stn_name, stn_lat, stn_long
    FROM stations
    WHERE stn_abbr = :st LIMIT 1
    ");
  $results->bindValue(':st',$st,PDO::PARAM_STR);
  $results->execute();
  $rows = $results->fetch(PDO::FETCH_ASSOC);
//	if (DEBUG){echo "in get_info: ";print_r($rows);echo "<br/>";}

  $dbh = null; // close connection
  $results = null;

  return $rows;
}


//--------------------------------------------------
//
// get_stn_abbr returns the abbrevation
// of a given station
//
function get_stn_abbr(&$st)
{
  $dbh = connect_to_db();

  $result = $dbh->prepare("
    SELECT stn_abbr
    FROM stations
    WHERE stn_name = :st LIMIT 1
    ");
  $result->bindValue(':st',$st,PDO::PARAM_STR);
  $result->execute();
  $row = $result->fetch(PDO::FETCH_ASSOC);
//	if (DEBUG){echo "in get_info: ";print_r($rows);echo "<br/>";}

  $dbh = null; // close connection
  $result = null;

  return $row["stn_abbr"];
}

?>