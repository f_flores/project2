<?php 
//------------------------------------
//
// Project 2 model controller
//
//------------------------------------
require_once("../includes/site_constants.php");

//if (DEBUG) echo "<br />In model.php<br />";

//-------------------------------------------------------------
// connect do user database
// set constants for database credentials
//
define('DB','p2maps');
define('DB_HOST','localhost');
define('DB_USER','ffweb');
define('DB_PASSWORD','ffweb');


//--------------------------------------------------------------
//
// Bart API urls
//
define('BAPI_KEY','MW9S-E7SL-26DU-VV8V');
define('BART_API','http://api.bart.gov/api/');
define('BART_RINFO_URI',BART_API.'route.aspx?cmd=routeinfo&key='.BAPI_KEY.'&route=');
define('BART_STNS_URI',BART_API.'stn.aspx?cmd=stns&key='.BAPI_KEY);
define('BART_ROUTES_URI',BART_API.'route.aspx?cmd=routes&key='.BAPI_KEY);
define('BART_ETD',BART_API.'etd.aspx?cmd=etd&key='.BAPI_KEY.'&orig=');
define('RXML','r_xml');

//$stations = __DIR__ . "../" . "html/" . "stations.xml";
//define('BART_RINFO_URI',
//'http://api.bart.gov/api/route.aspx?cmd=routeinfo&key=MW9S-E7SL-26DU-VV8V&route=');
//define('BART_STNS_URI','http://api.bart.gov/api/stn.aspx?cmd=stns&key=MW9S-E7SL-26DU-VV8V');
//define('BART_ROUTES_URI',
//'http://api.bart.gov/api/route.aspx?cmd=routes&key=MW9S-E7SL-26DU-VV8V');



//------------------------------------------------------------------
//
//  load_bartapp_data parses the stations information from the
//  xml file and places it into the stations database table
//
function load_bartapp_data()
{
		 $stations = setup_stations();
		 $rts = setup_routes();
		 $routes = get_routes();
     if (DEBUG){echo "routes: ";print_r($routes);}
     foreach($routes as $route)
     {
       foreach($route as $key=>$route_num)
       {
         $ruri = BART_RINFO_URI . $route_num;
				 //if (DEBUG) {echo "file $ruri <br />";}   
         load_route_data($ruri);
       }
     }
}

//------------------------------------------------------------------
//
//  setup_stations parses the stations information from the
//  xml file and places it into the stations database table
//
function setup_stations()
{
   if (!isset($st_xml))
	 {
		  $st_xml = simplexml_load_file(BART_STNS_URI);
      if (empty($st_xml))
      {
			  echo "Error loading BART station data!<br />";
      }
	 }

  foreach ($st_xml->xpath("/root/stations/station") as $item)
  {
    foreach ($item as $key=>$value)
    {
	     $stations["$key"] = $value;
			switch($key)
			{
			   case "name":
				   $sname = $value;
           break;
			   case "abbr":
					 $abbr = $value;
           break;
			   case "gtfs_latitude":
           $lat = $value;
           break;
			   case "gtfs_longitude":
           $longitude = $value;
           break;
			   case "address":
           $address = $value;
           break;
			   case "city":
				   $city = $value;
				   break;
			   case "zipcode":
				   $zipcode = $value;
				   break;
			   default:
				   break;
			}
    }
    $st_abbr = putin_stations($abbr,$sname,$lat,$longitude,$address,$city,$zipcode);
  }
  return $st_abbr;
}


//------------------------------------------------------------------
//
//  setup_routes parses the route information from the
//  xml file and places it into the route database table
//
function setup_routes()
{
	if (!isset($rts_xml))
	{
	  $rts_xml = simplexml_load_file(BART_ROUTES_URI);
    if (empty($rts_xml))
    {
			echo "Error loading BART route information!<br />";
    }
	}


  $routes = null;
  foreach ($rts_xml->xpath("/root/routes/route") as $item)
  {
    foreach ($item as $key=>$value)
    {
			switch($key)
			{
			   case "name":
				   $name = $value;
           break;
			   case "abbr":
					 $abbr = $value;
           break;
			   case "number":
           $number = $value;
           break;
			   case "color":
				   $color = $value;
				   break;
			   default:
				   break;
			}
    }
    $route_id = putin_routenames($number, $abbr, $name, $color); // for each item
  }
  return $routes;
}


//------------------------------------------------------------------
//
//  setup_rinfo parses the route information from the
//  xml file and places it into the route database table
//
function setup_rinfo(&$xml)
{
  //print_r($xml);
  //$routes = array();
 	$ruri = BART_RINFO_URI . "2";
  $r_xml = simplexml_load_file("$ruri");
  if (empty($r_xml))
  {
			echo "Error loading BART route information!<br />";
  }

  foreach ($r_xml->xpath("/root/routes/route") as $item)
  {
    foreach ($item as $key=>$value)
    {
			if ($key != "config")
      { 
	       $rinfo["$key"] = $value;
         if (DEBUG) {echo "Key: $key, Value: $value <br />";}
      }
      else
      {
				foreach($r_xml->xpath("/root/routes/route/config") as $path)
        {
          $i = 0; //initialize index variable
          foreach($path as $st=>$st_name)
          {
						 $st .= $i++;
             echo "$st: $st_name<br />";
             $rinfo["$st"] = $st_name;
					}
        }
      }
    }
  }
}


//----------------------------------------------------------
//
// putin_routenames inserts station abbr and station name
// in the route_names table, if duplicate the insert is
// not performed
//
function putin_routenames(&$roid,&$abbr,&$rname,&$color)
{
	$id_found = duplicate_row($abbr);
  if (DEBUG){echo "id_found in putin_routenames: $id_found<br/>";}
  if ($id_found == 0) // abbr not present in table so insert
  {
		$dbh = connect_to_db();
		if (DEBUG) echo "in putin_routenames: $roid, $abbr, $rname<br />";

		$dbh->beginTransaction();
		try 
		{
      // insert abbr, route_name and rourte id into table 
      $ins1 = $dbh->prepare("
        INSERT INTO route_names (roid,abbr,rname,color) 
        VALUES (:roid,:abbr,:rname,:color);
      ");
			$ins1->bindValue(':roid',(int)$roid,PDO::PARAM_INT);
      $ins1->bindValue(':abbr',$abbr,PDO::PARAM_STR);
      $ins1->bindValue(':rname',$rname,PDO::PARAM_STR);
      $ins1->bindValue(':color',$color,PDO::PARAM_STR);
      $ins1->execute();
      $dbh->commit();
		}
		catch(PDOException $e) 
		{
			echo $e->getMessage();
			$dbh->rollBack();
		}
		$ins1 = null;
    $dbh = null;
  }
	
  $odbh = connect_to_db();
  $ins2 = $odbh->prepare("     
    SELECT roid FROM route_names
    WHERE abbr=:abbr AND rname=:rname LIMIT 1
   ");
  $ins2->bindValue(':abbr',$abbr,PDO::PARAM_STR);
  $ins2->bindValue(':rname',$rname,PDO::PARAM_STR);
  $ins2->execute();
	$row = $ins2->fetch(PDO::FETCH_ASSOC);
  $ins2 = null;
  $odbh = null; 

 	return (empty($row["roid"])) ? 0 : $row["roid"]; 
}


//----------------------------------------------------------
//
// putin_stations inserts station abbr,station name,
// latitude and longitude coordinates, city name and zip 
// code in the stations table, if duplicate the insert is
// not performed
//
function putin_stations($abbr,$sname,$lat,$longitude,$address,$city,$zip)
{
	$stn_found = duplicate_station($abbr);
  if (DEBUG){echo "stn_found in putin_routenames: $stn_found<br/>";}
  if ($stn_found == null) // abbr not present in table so insert
  {
		$dbh = connect_to_db();
		if (DEBUG) echo "in putin_stations: $abbr, $sname,$lat,$longitude,$city,$zip<br />";

		$dbh->beginTransaction();
		try 
		{
      // insert abbr, route_name and rourte id into table 
      $ins1 = $dbh->prepare("
        INSERT INTO stations 
        (stn_abbr,stn_name,stn_lat,stn_long,stn_addr,stn_city,stn_zipcode) 
        VALUES (:abbr,:sname,:lat,:longitude,:address,:city,:zip);
      ");
      $ins1->bindValue(':abbr',$abbr,PDO::PARAM_STR);
      $ins1->bindValue(':sname',$sname,PDO::PARAM_STR);
      $ins1->bindValue(':lat',$lat,PDO::PARAM_STR);
      $ins1->bindValue(':longitude',$longitude,PDO::PARAM_STR);
      $ins1->bindValue(':address',$address,PDO::PARAM_STR);
      $ins1->bindValue(':city',$city,PDO::PARAM_STR);
      $ins1->bindValue(':zip',$zip,PDO::PARAM_STR);
      $ins1->execute();
      $dbh->commit();
		}
		catch(PDOException $e) 
		{
			echo $e->getMessage();
			$dbh->rollBack();
		}
		$ins1 = null;
    $dbh = null;
  }

  $odbh = connect_to_db();
  $ins2 = $odbh->prepare("     
    SELECT stn_abbr FROM stations 
    WHERE stn_abbr=:abbr LIMIT 1
   ");
  $ins2->bindValue(':abbr',$abbr,PDO::PARAM_STR);
  $ins2->execute();
	$row = $ins2->fetch(PDO::FETCH_ASSOC);
  $ins2 = null;
  $odbh = null; 

 	return (empty($row["stn_abbr"])) ? null : $row["stn_abbr"]; 
}


//--------------------------------------------
//
// duplicate_row returns 0 if route_id is
// already in table, otherwise returns 
// route_id of existing symbol.
// in database (avoiding duplicates)
//
function duplicate_row(&$v1)
{
  $dbh = connect_to_db();
  if (DEBUG) echo "<br />duplicate_row<br />";

  $search_id   = $dbh->prepare("
    SELECT roid FROM route_names
    WHERE abbr=:v1 LIMIT 1
   ");
   
  $search_id->bindValue(':v1',$v1,PDO::PARAM_STR);
  $search_id->execute();
  $row = $search_id->fetch(PDO::FETCH_ASSOC);

  echo "<br />route_id in duplicate_row: " . $row["roid"] . "<br />";  
  //close connections
  $search_id = null;
  $dbh = null;

 	return (empty($row["roid"])) ? 0 : $row["roid"]; 
}


//--------------------------------------------
//
// duplicate_station returns 0 if station abbrevation
// is already in table, otherwise returns 
// station 'abbr' of existing symbol.
//
function duplicate_station(&$v1)
{
  $dbh = connect_to_db();
  if (DEBUG) echo "<br />duplicate_row<br />";

  $search_id   = $dbh->prepare("
    SELECT stn_abbr FROM stations
    WHERE stn_abbr=:v1 LIMIT 1
   ");
   
  $search_id->bindValue(':v1',(string)$v1,PDO::PARAM_STR);
  $search_id->execute();
  $row = $search_id->fetch(PDO::FETCH_ASSOC);

  echo "<br />station_id in duplicate_station: " . $row["stn_abbr"] . "<br />";  
  //close connections
  $search_id = null;
  $dbh = null;

 	return (empty($row["stn_abbr"])) ? null : $row["stn_abbr"]; 
}


//------------------------------------------
//
// connect_to_db is a routine that connects
// to the mysql database via PDO
// the function returns a 'handle' to the
// database
//
function connect_to_db()
{
  $pdo_db = "mysql:host=".DB_HOST.";dbname=".DB;

 // connect to database
  try
	{
    $dbh = new
			 PDO($pdo_db,DB_USER,DB_PASSWORD,array(
				 PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION,
				 PDO::ATTR_EMULATE_PREPARES=> false
			 ));
	}
  catch (PDOException $e)
  {
		echo 'Connection failed:  ' . $e->getMessage();
  }
  return $dbh;
}


// store multiple xpath queries for menu into one array
// of variable arrays for processing by model.php
// originally call from controller
//   $menu_data = array( 
//  	  "category" => "(/menu/category['name'])",
//      "item" => "(/menu/category/item['type'])",
//   );


$mpath =  __DIR__ . "/../" . MODEL;

//  print("<p>In Model module.</p>");
//$menuxml = simplexml_load_file($mpath . "menu2.xml");


// Support multiple xpath queries at once
// menu_data holds multiple arrays. Each array associated with an
// xml variable name holds a list of values of the xpath queries. 
/*
foreach( $menu_data as $variable_name => $xpath ) 
{
		 $menu_data["$variable_name"] = $menuxml->xpath($xpath);
}
  foreach 
  ($mxml->xpath("/menu/category[@name='$category']/item") as $item)


function test_function_pxml(&$menuxml) {
	print_r($menuxml);
}
*/
/*
//--------------------------------------------
//
// insert values into users table and names
// table, returns user_id
//
function insert_in_db(&$email,&$password,&$fname,&$lname,&$cash)
{
  $dbh = connect_to_db();
  if (DEBUG) echo "<br />insert_in_db<br />";

  $dbh->beginTransaction();
  try {
  // insert email and password into users table 
    $ins1 = $dbh->prepare("
      INSERT INTO users (email,password) 
      VALUES(:email,:password);
    ");
    $ins1->bindValue(':email',$email,PDO::PARAM_STR);
    $ins1->bindValue(':password',$password,PDO::PARAM_STR);
    $ins1->execute();
    $dbh->commit();
  }
  catch(PDOException $e) 
  {
		echo $e->getMessage();
    $dbh->rollBack();
	}

  $ins2 = $dbh->prepare("     
    SELECT id FROM users
    WHERE LOWER(email)=:email AND password=:password LIMIT 1
   ");
  $ins2->bindValue(':email',$email,PDO::PARAM_STR);
  $ins2->bindValue(':password',$password,PDO::PARAM_STR);
  $ins2->execute();
	$row = $ins2->fetch(PDO::FETCH_ASSOC);
  $ins2 = null;
  $ins1 = null;
  $dbh = null; 
  $uid = $row["id"];

  $odbh = connect_to_db();
  $odbh->beginTransaction();
  // insert foreign key 'uid' into names
  try
  {
    $ins3 = $odbh->prepare("
      INSERT INTO names (first_name,last_name,cash_avlbl,user_id) 
      VALUES(:fname,:lname,:cash,:uid)
     ");
    $ins3->bindValue(':fname',$fname,PDO::PARAM_STR);
    $ins3->bindValue(':lname',$lname,PDO::PARAM_STR);
    $ins3->bindValue(':cash',$cash,PDO::PARAM_INT);
    $ins3->bindValue(':uid',$uid,PDO::PARAM_INT);
    $ins3->execute();
    $odbh->commit();
	}
  catch(PDOException $e) 
  {
		echo $e->getMessage();
    $odbh->rollBack();
	}

  // close connections
  $odbh = null;
  $ins3 = null;

  return $uid;
}


//--------------------------------------------
//
// is_duplicate returns true is email is already
// in database (avoiding duplicates)
// note that database and table is already open
//
function is_duplicate($eml)
{
  $dbh = connect_to_db();

  $search_user = $dbh->prepare('
    SELECT id FROM users
    WHERE LOWER(email)=:email LIMIT 1
   ');
   
  $search_user->bindValue(':email',$eml,PDO::PARAM_STR);
  $search_user->execute();

  $id_found = search_val($search_user,"id");

  // close data connections
  $search_user = null;
  $dbh = null;

  return ($id_found > 0) ? true : false;
}


//--------------------------------------------
//
// is_duplicate_sym returns 0 if symbol and
// stock name are already in table, 
// otherwise returns id of existing symbol.
// in database (avoiding duplicates)
// note that database and table is already open
//
function is_duplicate_sym($s,$n)
{
  $dbh = connect_to_db();
  if (DEBUG) echo "<br />is_duplicate_sym<br />";
  $id_found = 0;
  $search_id   = $dbh->prepare("
    SELECT id FROM stock_names
    WHERE stock_name=:n AND stock_symbol=:s LIMIT 1
   ");
   
  $search_id->bindValue(':n',$n,PDO::PARAM_STR);
  $search_id->bindValue(':s',$s,PDO::PARAM_STR);
  $search_id->execute();
  $rows = $search_id->fetch(PDO::FETCH_ASSOC);
  
  //close connections
  $search_id = null;
  $dbh = null;

 	return (empty($rows['id'])) ? 0 : $rows['id']; 
}


//--------------------------------------------------
//
// get_sinfo returns row matching stock_id
//  row contains stock symbol and stock name
//  available
//
function get_sinfo(&$sid)
{
  $dbh = connect_to_db();

  $results = $dbh->prepare('
    SELECT id,stock_symbol,stock_name 
    FROM stock_names
    WHERE id = :sid LIMIT 1');
  $results->bindValue(':sid',$sid,PDO::PARAM_INT);
  $results->execute();
  $row = $results->fetch(PDO::FETCH_ASSOC);

  $dbh = null; // close connection
  $results = null;

  return $row;
}

 */

?>