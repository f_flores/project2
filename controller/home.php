<?php 
//------------------------------------
//
// Bart app Home Page controller
// Project 2
//
//------------------------------------

//require_once("../model/model.php");
require_once("../includes/tools.php");
require_once("../model/model.php");
require_once("../model/model_tools.php");

$local_url = $_SERVER["PHP_SELF"] . "?page=" . $pg;


if (isset($_SESSION['authenticated']))
{
  if ($_SESSION['authenticated'])
  {
    /***
		 // if database is not setup then retreive info and populate db_tables
     $load_bartapp_data();
		****/
		
		 if (isset($_GET["route"]))
		 {
		   $rnum = htmlspecialchars($_GET["route"]);
       if (DEBUG){echo "route number: $rnum<br />";}
//     sleep(5); // pretend server is slow
       $results["name"] = "bob";
       $results["route"] = $rnum;
		   render("home",['results'=>$results]);
     }
     else
     {
       render("home");
     }
  }
}
else
  render("home");


?>