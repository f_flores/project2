<?php 
//session_start();
//------------------------------------
//
// Bart app Route Page controller
// Project 2
//
//------------------------------------

//require_once("../model/model.php");
require_once("../includes/tools.php");
require_once("../model/model.php");
require_once("../model/model_tools.php");

$local_url = $_SERVER["PHP_SELF"] . "?page=" . $pg;

//if (DEBUG) {echo "Session auth: " . $_SESSION["authenticated"] . "<br />";}
//defines route 
class Route
{
	public $abbr;
  public $rname;
  public $color;
  public $nstations;
  public $path_list;
	//  public $coords;  
}

if (isset($_SESSION['authenticated']))
{
  if ($_SESSION['authenticated'])
  {
    /****
		 // if database is not setup then retreive info 
     // and populate db_tables
     load_bartapp_data();
		****/

		
		 if (isset($_GET["route"]))
		 {
		   $rnum = htmlspecialchars($_GET["route"]);
//       if (DEBUG){echo "route number: $rnum<br />";}
		   // set MIME type
			 header("Content-type: application/json");
       $this_r = new Route();
       $rinfo = get_rinfo($rnum);
			 /*	if (DEBUG){echo "in route.php: ";print_r($rinfo);
					echo '<br />abbr $rinfo["abbr"]<br/>';} */

       foreach ($rinfo as $k=>$v)
			 {
				 $this_r->$k = $v;
			 }
       $st_info = get_stinfo($rnum);
       $this_r->nstations = $st_info["nstations"];
       $st_list = str_getcsv($st_info["path_list"],",");
       $p_list = null;
       foreach ($st_list as $k=>$v)
			 {
         //if (DEBUG){echo "st_list k: $k, v: $v<br />";}
				 //$p_list = $v;
         $stn = $v;
         $pt_stn = get_stcoords($stn);
         $p_list[$v] = array("stn_abbr"=>$pt_stn["stn_abbr"],
                             "stn_name"=>$pt_stn["stn_name"],
                             "stn_lat"=>$pt_stn["stn_lat"],
                             "stn_long"=>$pt_stn["stn_long"]); 
			 }
       $this_r->path_list = $p_list;

			 //if (DEBUG){echo "in controller/route.php: ";print_r($st_list); echo '<br />abbr $rinfo["abbr"]<br/>';}

			 // output JSON
			 print(json_encode($this_r));
       //render("route",['results'=>$this_r]);
     }
     else
     {
       render("route");
     }
  }
}
else
  render("route");


?>