<?php 
//session_start();
//------------------------------------
//
// Bart app Route Page controller
// Project 2
//
//------------------------------------

//require_once("../model/model.php");
require_once("../includes/tools.php");
require_once("../model/model.php");
require_once("../model/model_tools.php");

$local_url = $_SERVER["PHP_SELF"] . "?page=" . $pg;

//if (DEBUG) {echo "Session auth: " . $_SESSION["authenticated"] . "<br />";}
//defines route 
class Departure
{
	public $dstn;
  public $dep_list;
}


if (isset($_SESSION['authenticated']))
{
  if ($_SESSION['authenticated'])
  {
    /****
		 // if database is not setup then retreive info 
     // and populate db_tables
     load_bartapp_data();
		****/

		
		 if (isset($_GET["station"]))
		 {
		   $stn = htmlspecialchars($_GET["station"]);
			 $dinfo = get_dinfo($stn);
			 header("Content-type: application/json");
       $this_s = new Departure();
       $this_s->dstn = get_stn_name($stn);
			 $this_s->dep_list = $dinfo; 
			 print(json_encode($this_s));
       $this_s = null;
       unset($this_s);
     }
     else
     {
       render("route");
     }
  }
}
else
  render("route");


?>