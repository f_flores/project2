<?php 
//-------------------------------------------
//
//  View module for route page  
//
//-------------------------------------------

render("header",['title'=>'CS75 Bart App',
                 'css_path'=>INET_CSS_PATH,
                 'css_file'=>'style.css']);
//if (DEBUG) {echo "view from route.php<br />";}
?>

<body onload="initialize();">
  <h2>Bart Map Interactive Menu</h2>
	<div id="map_canvas"></div>
    <div class="routes_tb">
			Select Route:
      <select name="route" id="sel_route" onchange="broute();">
<?php 
// drop down menu which displays BART routes
   $rlist = get_routes(); 
// if (DEBUG) {print_r($rlist);}
   $first_opt = true;
   foreach ($rlist as $robj) 
   { 
     foreach ($robj as $rn=>$rv)
     {
       $rinfo = get_rinfo($rv);
       if ($first_opt)
       {
				 echo "<option value=\"\" disable selected>Choose a route</option>";
         $first_opt = false;
       }
?>
			 <option value="<?=$rv?>">Route <?=$rv?>&nbsp;<?=$rinfo["rname"]?></option> 
<?php
     }
   } 
?> 
      </select>
    <div id="rte_output">
    </div>
  </div>
  <div id="dyn_schedule"></div>
<?php 
  render("footer");
?>
