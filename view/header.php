<?php 
require_once("../includes/site_constants.php");
?>
<!DOCTYPE html>
<!-- Standard header of html page -->
<?php if (isset($lang)) { ?> 
<html lang="<?= $lang ?>">
<?php } else { ?>
<html lang="en-US">
<?php } ?>
<head>
<?php  if (isset($ch_set)) { ?>
    <meta charset="<?= htmlspecialchars($ch_set); ?>">
<?php }  else  { ?>
    <meta charset="UTF-8">
<?php } ?>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <title><?php if (isset($title))print (htmlspecialchars($title)); ?>
    </title>
    <link rel="stylesheet" href="<?= BOOTSTRAPCSS ?>" />
    <script type="text/javascript" src="<?= JQUERYJS ?>"></script>
    <script type="text/javascript" src="<?= BOOTSTRAPJS ?>"></script>
<?php // handle stylesheet
    if (isset($css_file) && isset($css_path)) { 
        $href_file = $css_path . $css_file; ?>
        <link rel="stylesheet" type="text/css" href="<?= $href_file ?>"/>
<?php  }  ?>
    <script async defer type="text/javascript" src="<?= GMAP ?>"></script>
    <script type="text/javascript" src="/js/p2maps.js"></script>
</head>
<?php if (isset($_SESSION["authenticated"])) { ?>
<?php 
								 /*******
  <header>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
         <a class="navbar-brand" href="#">C$75 Finance</a>
        </div>
      <ul class="nav navbar-nav">
          <li><a href="home">Home</a></li>
          <li><a href="quote">Quotes</a></li>
          <li><a href="portfolio">Portfolio</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
<li><p class="navbar-text">Logged in as 
								 ********/
?>
<?php 
								 /***
		 $row = get_names($_SESSION['userid']);
     $name = $row['first_name'] . " " . $row['last_name'];
     echo $name; 
								 ***/
?>
<?php 
								 /****
</p></li>
 <li><a href="logout"><span class="glyphicon glyphicon-log-out"></span>
     Logout</a></li>
      </ul>
      </div>
    </nav>
  </header>
								 ****/
?>
<?php } else { ?>
<?php 
								 /*******
  <header>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
         <a class="navbar-brand" href="#">C$75 Finance</a>
        </div>
      <ul class="nav navbar-nav">
<li><a href="register"><span class="glyphicon glyphicon-user"></span>
     Sign Up</a></li>
<li><a href="login"><span class="glyphicon glyphicon-log-in"></span>
     Login</a></li>
      </ul>
    </nav>
  </header>
								 *****/
?>
<?php } ?>
