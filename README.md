San Francisco BART Schedule

This project provides an interface to the BART schedule. 

Users of the app can select a stop on the google map and view the subway trains, on the different routes, arriving at the selected station. 

For this project, I leveraged PHP, MySQL and javascript. Also, I used the google maps API and the BARTS API, which contains realtime route schedules.