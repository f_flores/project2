//----------------------------------------------------------
//
// Javascript functions for Project2 Bart maps
//
//----------------------------------------------------------
/*jslint browser: true */
/*jslint devel: true */
/*global google */


"use strict";

// globals for bart app
var SF_LAT = 37.775362,    //should be const in js 6
    SF_LONG = -122.417564, //should be const in js 6
    DEFAULT_ZOOM = 11,     //should be const in js 6
    SMALL_STR = 7,
    MEDIUM_STR = 12,
    BIG_STR = 20,
    map = "",
    marker = "",
    xhr = null,
    xhrd = null,
    rte_container = document.getElementById("rte_output"),
    route_color = '#000000',
    infowindow_str = "",
    prev_infowindow = false,
    dep_out = null;


//----------------------------------------------------------
//
// initialize map -- based on section notes
//     San Francisco as center of map
//
function initialize() {
    var center = {lat: SF_LAT, lng: SF_LONG};

    map = new google.maps.Map(document.getElementById('map_canvas'),
          {
            zoom: DEFAULT_ZOOM,
            center: center
        });
}


//----------------------------------------------------------
//
// init_rmap -- initializes map with selected route's  
//   "halfway point" as center
//
function init_rmap(n, list) {
    var halfway = Math.floor(n / 2), plat, plng, index = 1, point = "";

    for (point in list)
        {
            if (list.hasOwnProperty(point)) {
                if (index === halfway) {
                    plat = parseFloat(list[point].stn_lat);
                    plng = parseFloat(list[point].stn_long);
                    break; // out of for loop
                }
                index += 1;
            }
        }

    map = new google.maps.Map(document.getElementById('map_canvas'),
          {
            zoom: DEFAULT_ZOOM,
            center: {lat: plat, lng: plng}
        });
}


//----------------------------------------------------------
//
// get_deptime next departure time
//
function get_deptime(stn_abbr) {
    var url = "";
    // instantiate xhttp request
    try {
        xhrd = new XMLHttpRequest();
    } catch (e) {
        xhrd = new window.ActiveXObject("Microsoft.XMLHTTP");
    }

    // handle legacy browsers 
    if (xhrd === null) {
        alert("Ajax not supported by your browser.");
    }

    url = "departure.php?station=" + stn_abbr;
    xhrd.onreadystatechange =
        function () {
            var key = "", content_string = "";
      //*** source taken from lecture 7 notes 'ajax10.html'
      // only handle requests which loaded correctly
            if (xhrd.readyState === 4) {
                if (xhrd.status === 200) {
                    dep_out = JSON.parse(xhrd.responseText);
                    /**** Following 3 lines of code for debugging purposes
                    console.log("dep_out.dstn: " + dep_out.dstn);
                    console.log("dep_out.dep_list: " + dep_out.dep_list);

                    console.log("url: " + url);
                     ****/
                    document.getElementById("dyn_schedule").style.backgroundColor = route_color;
                    document.getElementById("dyn_schedule").style.color = "black";
                    content_string += "<table class = \"dschedule\"><tr><th>DEST</th><th>DEPARTING IN</th></tr>";
                    infowindow_str += "<h6 style='background-color:" + route_color + "'>Station: " + dep_out.dstn + "<br />";
                    infowindow_str += "<table><tr><th>DEST</th><th>Departing in</th></tr>";
                    for (key in dep_out.dep_list)
                        {
                            if (dep_out.dep_list.hasOwnProperty(key)) {
                                //** debugging console.log("dkey: " + key + key.length + " t: " + dep_out.dep_list[key]);
                                content_string += "<tr><td>" + key + "</td><td>" + dep_out.dep_list[key] + "</td></tr>";
                                infowindow_str += "<tr><td>" + key + "</td><td>" + dep_out.dep_list[key] + "</td></tr>";
                            }
                        }
                    content_string += "</table>";
                    infowindow_str += "</table>";
                    document.getElementById("dyn_schedule").style.background = route_color;
                    document.getElementById("dyn_schedule").innerHTML = "Station: " + dep_out.dstn + " Departure Times";
                    document.getElementById("dyn_schedule").innerHTML += content_string;
                    document.getElementById("infow").innerHTML = infowindow_str;
                    content_string = "";
                } else {
                    alert("Error with Ajax call!");
                }
            }
        };
    // get destination
    xhrd.open("GET", url, true);
    xhrd.send(null);
}



//----------------------------------------------------------
//
// add_info_window -- adds info window
//
function add_info_window(mk, abbr) {
    var infowindow;

    mk.addListener('click', function () {
        if (prev_infowindow) {
            prev_infowindow.close();
        }

        get_deptime(abbr);

        infowindow = new google.maps.InfoWindow({
            // the infow div lets the google map info window be updated dynamically
            content: "<div id=\"infow\">" + infowindow_str + "</div>"
        });
        infowindow.open(map, mk);
        prev_infowindow = infowindow;
        infowindow_str = ""; // reset info window content string
    });
}


//----------------------------------------------------------
//
// add_marker -- adds marker
//
function add_marker(abbr, stn, lat, lng) {
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        title: stn
    });
    marker.setMap(map);

    add_info_window(marker, abbr);
}


//----------------------------------------------------------
//
// add_polyline -- draws a line from point (a,b) to point 
// (c,d) the points represents stations on the bart route
//
function add_polyline(a, b, c, d) {
    var poly_coords = [
            new google.maps.LatLng(a, b),
            new google.maps.LatLng(c, d)
        ],
        poly_lpath = new google.maps.Polyline({
            path: poly_coords,
            geodesic: true,
            strokeColor: route_color,
            strokeOpacity: 1.0,
            strokeWeight: 3
        });
    poly_lpath.setMap(map);
}


//----------------------------------------------------------
//
// expand_path -- traverses the path list object and adds
// route's station markers to list
//
function expand_path(data) {
    var htmlString = "", key = "", prv_lat, prv_long, i = 1;

    for (key in data) {
        if (data.hasOwnProperty(key)) {
            htmlString += "<p>Station " + i + ": " + data[key].stn_name +
                          "</p>";
            // console.log("Key: " + key + " htmlString: " + htmlString);
            // put data in dep_out global variable at beginning of app
            // "edge" case of first marker info window
/***
            if (dep_out === null) {
                get_deptime(data[key].stn_name);
            }
****/
            add_marker(data[key].stn_abbr, data[key].stn_name, data[key].stn_lat, data[key].stn_long);
            if (prv_lat !== "undefined") {
                add_polyline(prv_lat, prv_long,
                            data[key].stn_lat, data[key].stn_long);
            }
            prv_lat = data[key].stn_lat;
            prv_long = data[key].stn_long;
            i += 1;
        }
    }
    document.getElementById("rte_output").innerHTML += htmlString;
}


//----------------------------------------------------------
//
// broute() -- broute
//
function broute() {
    var route = document.getElementById("sel_route").value, url = "";
    // instantiate xhttp request
    try {
        xhr = new XMLHttpRequest();
    } catch (e) {
        xhr = new window.ActiveXObject("Microsoft.XMLHTTP");
    }

    // handle legacy browsers 
    if (xhr === null) {
        alert("Ajax not supported by your browser.");
    }

    // get route number
    url = "route.php?route=" + route;
    xhr.onreadystatechange =
        function () {
      //*** source taken from lecture 7 notes 'ajax10.html'
      // only handle requests which loaded correctly
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var rte_out = JSON.parse(xhr.responseText), div = "", text = "", nstn = rte_out.nstations, plist = rte_out.path_list;
                    init_rmap(nstn, plist); //remove previous polyline and markers if any
                    route_color = rte_out.color.toUpperCase();
                    document.getElementById("rte_output").style.fontSize = '12pt';
                    document.getElementById("rte_output").innerHTML = "";
                    // show JSON in text area
                    //insert route information into DOM
                    div = document.createElement("div");
                    div.style.color = "black";
                    div.style.background = route_color;
                    text = document.createTextNode("Selected Route: " + rte_out.abbr);
                    div.appendChild(text);
                    div.appendChild(document.createElement("br"));
                    text = document.createTextNode("Route: " + rte_out.rname);
                    div.appendChild(text);
                    div.appendChild(document.createElement("br"));
                    text = document.createTextNode("Number of stations: " + rte_out.nstations);
                    div.appendChild(text);
                    document.getElementById("rte_output").appendChild(div);
                    expand_path(plist);
                } else {
                    alert("Error with Ajax call!");
                }
            }
        };
    xhr.open("GET", url, true);
    xhr.send(null);

}
