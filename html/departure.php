<?php
//--------------------------------------------
// 
// Project 2 departure
//
//------------------------------------------- 

  require_once("../includes/site_constants.php");
  require_once("../includes/tools.php");

  // find out if session started, otherwise
  // go to home page   
  if (isset($_GET["pg"]))
     $pg = htmlspecialchars($_GET["pg"]);
  else
     $pg = "departure";

  $path =  __DIR__ . "/../" . CONTROLLER . $pg . ".php";
  $_SESSION["authenticated"]=true; // for now
  // check for file existence
  if (file_exists($path))
		require($path);

?>
