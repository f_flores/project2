<?php
//--------------------------------------------
//  renderizar encabezado o pie de pagina
//  basado en curso cs75
//
function render($template, $data = [])
{
  	$path = __DIR__ . "/../" . VIEW . $template . ".php";
    if (file_exists($path))
    {
      extract($data);
      require($path);
    }
}

//--------------------------------------------
//
// redirect browser to URL indicated in Location
//
function redirect($file)
{
	header('HTTP/1.1 301 Moved Permanently');
  $urlpath = INET_DIR;
  header("Location: $urlpath/$file");
  exit();
}
?>
