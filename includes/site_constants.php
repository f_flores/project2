<?php 
// *********************************************************
// * File name: site_constants.php
// * Description: Definition of site wide constant variables
// * 
// *********************************************************
define("DEBUG", true);


// *
// * SITE_DIR is relative from Apache Configuration file
// * "Document Root". In this case, one level up:
// * "/var/www/project2"
// *
define("SITE_DIR", dirname(dirname(__FILE__)));

/*
 * INET_DIR is 'absolute' internet path
 * used to locate css templates
 */
define("INET_DIR",$_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST']);

/*
 * GMAP defines google maps api url
 * BOOTSTRAPCSS is the url to bootstrap's cdn css library
 * BOOTSTRAPJS is bootstrap's javascript url
 * JQUERYJS is the url to google's ajax jquery api
 *
 */
define("GAPI_KEY","AIzaSyDKJreVYFBCxvTVXx49jYICLWuTouQ4c0U");
//define("GMAP","http://maps.googleapis.com/maps/api/js?sensor=false");
define("GMAP","http://maps.googleapis.com/maps/api/js?key=".GAPI_KEY);
//define("BOOTSTRAPCSS","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css");
//define("BOOTSTRAPJS","https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js");
//define("JQUERYJS","https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js");


/* set locale */
setlocale(LC_MONETARY, 'en_US.UTF-8');

// *
// * MVC model Directory names
// *
define("CONTROLLER","controller/");
define("VIEW","view/");
define("MODEL","model/");
define("IMGS", "imgs/");
define("CSS", "css/");
define("INCLUDES","includes/");
define("LIBS","libs/");


// *
// * Relative paths to MVC and site folders
// *
define("CONTROLLER_PATH", SITE_DIR . "/" . CONTROLLER);
define("VIEW_PATH", SITE_DIR . "/" . VIEW);
define("MODEL_PATH", SITE_DIR . "/" . MODEL);
define("INCLUDES_PATH", SITE_DIR . "/" . INCLUDES);
define("INET_CSS_PATH", INET_DIR . "/" . CSS);
define("INET_IMG_PATH", INET_DIR . "/" . IMGS);

define("BOOTSTRAPCSS", "/" . LIBS . "bootstrap.min.css");
define("BOOTSTRAPJS", "/" . LIBS . "bootstrap.min.js");
define("JQUERYJS", "/" . LIBS . "jquery.min.js");


?>
